<?php

/**
 * @file
 * Admin things for transfer user content.
 */

/**
 * The admin page callback.
 */
function transfer_user_content_transfer_form($form, $form_state, $account) {
  $form['account1'] = array(
    '#type' => 'value',
    '#value' => $account,
  );
  $form['new_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Transfer content to:'),
    '#description' => t('Select a user to transfer the content to'),
    '#maxlength' => 60,
    '#autocomplete_path' => 'user/autocomplete',
  );
  $form['actions'] = array(
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Transfer content'),
    ),
    'cancel' => array(
      '#type' => 'markup',
      '#markup' => l(t('Cancel'), 'user/' . $account->uid),
    ),
  );
  return $form;
}

/**
 * Validate transfer_user_content_transfer_form submissions.
 *
 * @see transfer_user_content_transfer_form()
 */
function transfer_user_content_transfer_form_validate($form, &$form_state) {
  $new_account = user_load_by_name($form_state['values']['new_user']);
  if (!$new_account) {
    form_set_error('new_user', t('Error loading a user with that name'));
  }
  $form_state['values']['account2'] = $new_account;
}

/**
 * Form submission handler for transfer_user_content_transfer_form().
 *
 * @see transfer_user_content_transfer_form()
 */
function transfer_user_content_transfer_form_submit($form, &$form_state) {
  $form_state['redirect'] = 'user/' . $form_state['values']['account1']->uid . '/transfer_content/' . $form_state['values']['account2']->uid;
}

/**
 * Confirm for for this.
 *
 * @see transfer_user_content_transfer_form_confirm_submit()
 */
function transfer_user_content_transfer_form_confirm($form, $form_state, $account1, $account2) {
  if (!$account1 || !$account2 || !is_object($account1) || !is_object($account2)) {
    return t('There seems to be an error. One of the users in question might have been deleted or blocked?');
  }
  $form['account1'] = array(
    '#type' => 'value',
    '#value' => $account1,
  );
  $form['account2'] = array(
    '#type' => 'value',
    '#value' => $account2,
  );
  return confirm_form(
    $form,
    t('Are you sure you want to do this?'),
    'user/' . $account1->uid . '/contact',
    t('This will permanently move all content from @user1 to @user2', array(
      '@user1' => $account1->name,
      '@user2' => $account2->name,
    )),
    t('Move content'),
    t('Cancel')
  );
}

/**
 * Form submission handler for transfer_user_content_transfer_form_confirm().
 *
 * @see transfer_user_content_transfer_form_confirm()
 */
function transfer_user_content_transfer_form_confirm_submit($form, &$form_state) {
  $operations = array();
  $account1 = $form_state['values']['account1'];
  $account2 = $form_state['values']['account2'];
  $form_state['redirect'] = 'user/' . $account1->uid;

  $rows = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('n.uid', $account1->uid)
    ->addTag(TRANSFER_USER_CONTENT_QUERY_TAG)
    ->execute();

  $nids = array();

  foreach ($rows as $row) {
    // Add an operation here.
    $nids[] = $row->nid;
  }

  // Process in batches of 10.
  foreach (array_chunk($nids, 10) as $nid_group) {
    $operations[] = array(
      'transfer_user_content_transfer_chunk',
      array(
        $nid_group,
        $account1->uid,
        $account2->uid,
      ),
    );
  }

  $batch = array(
    'operations' => $operations,
    'finished' => 'transfer_user_content_transfer_finished',
  );

  batch_set($batch);
}
